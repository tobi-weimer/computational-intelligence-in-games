﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SabberStoneCore.Tasks;

namespace SabberStoneCoreAi.Agent
{
	class RollingHorizonAgent : AbstractAgent
	{
		private Random Rnd = new Random();
		private Stopwatch LongestTurnStopwatch = new Stopwatch();

		private POGame.POGame InitialPoGame;

		/// <summary>
		/// List-PlayerTask - Hero Killed - Hero Life Steal - Killed Minions
		/// </summary>
		private Dictionary<int?, Tuple<List<PlayerTask>, bool, int, int>> OptionsDictionary;
		private int? choosenTaskList;
		private int moveNum;
		private int recusionDepth = 0;

		public override void FinalizeAgent()
		{
			Console.WriteLine("MyFirstAgent Finalize Agent");
			// TODO implement
			// Nothing to do here yet
		}

		public override void FinalizeGame()
		{
			Console.WriteLine("MyFirstAgent Finalize Game");
			Console.WriteLine("The Longest Turn took {0} ms", LongestTurnStopwatch.ElapsedMilliseconds);
			// TODO implement
			// Nothing to do here yet
		}


		public override void InitializeAgent()
		{
			Console.WriteLine("MyFirstAgent Init Agent");
			Rnd = new Random();
			// TODO implement
		}

		public override void InitializeGame()
		{
			Console.WriteLine("MyFirstAgent Init Game");
			// TODO implement
			// Nothing to do here yet
		}

		private void FillTaskLists(POGame.POGame poGame, List<PlayerTask> pTaskList)
		{
			recusionDepth++;
			List<PlayerTask> options = poGame.CurrentPlayer.Options();

			// add EndTurn Options and add Lists to Dictionary
			if (options.Count == 1 )
			{
				List<PlayerTask> myTaskList = new List<PlayerTask>(pTaskList);

				AddEntryToOptionsDictionary(myTaskList, poGame);
			}

			List<PlayerTask> myOptions = getNotEndTurnTasks(options);
			Dictionary<PlayerTask, POGame.POGame> results = poGame.Simulate(myOptions);

			// createTaskLists
			foreach (PlayerTask aPlayerTask in myOptions)
			{
				List<PlayerTask> myTaskList = new List<PlayerTask>(pTaskList);
				myTaskList.Add(aPlayerTask);

				// add further tasks
				POGame.POGame aPoGame = results.GetValueOrDefault(aPlayerTask);

				if (MyStopwatch.ElapsedMilliseconds < 70000 && recusionDepth < 10)
				{
					FillTaskLists(aPoGame, myTaskList);
				}
			}
			recusionDepth--;
		}


		/// <summary>
		/// get the next move using Rolling Horizon
		/// </summary>
		private PlayerTask getRHMove(POGame.POGame poGame, List<PlayerTask> options)
		{
			PlayerTask choosenTask = null;

			if (options.Count == 1)
			{
				choosenTask = options[0];
			}
			else
			{
				if (firstMoveThisTurn)
				{
					OptionsDictionary = new Dictionary<int?, Tuple<List<PlayerTask>, bool, int, int>>();
					moveNum = 0;

					List<PlayerTask> newTaskList = new List<PlayerTask>();
					FillTaskLists(poGame, newTaskList);

					choosenTaskList = SelectTaskList();
					firstMoveThisTurn = false;
				}

				Tuple<List<PlayerTask>, bool, int, int> anEntry = OptionsDictionary.GetValueOrDefault(choosenTaskList);
				if (anEntry != null)
				{
					List<PlayerTask> choosenList = anEntry.Item1;
					if (choosenList != null && moveNum >= 0 && moveNum < choosenList.Count)
					{
						choosenTask = choosenList[moveNum++];
					}
				}
			}

			if (choosenTask == null)
			{
				List<PlayerTask> myOptions = getNotEndTurnTasks(options);
				choosenTask = myOptions[Rnd.Next(myOptions.Count)];
			}

			return choosenTask;
		}

		private List<PlayerTask> getNotEndTurnTasks(List<PlayerTask> options)
		{
			List<PlayerTask> myOptions = new List<PlayerTask>(options.Count);
			foreach (PlayerTask aPlayerTask in options)
			{
				if (!aPlayerTask.PlayerTaskType.Equals(PlayerTaskType.END_TURN))
				{
					myOptions.Add(aPlayerTask);
				}
			}
			return myOptions;
		}

		private int currentTurn = -1;
		private bool firstMoveThisTurn = false;

		private Stopwatch MyStopwatch;
		private int cardsPlayedThisTurn;

		public override PlayerTask GetMove(POGame.POGame poGame)
		{
			InitialPoGame = poGame;

			if (poGame.Turn != currentTurn)
			{
				MyStopwatch = Stopwatch.StartNew();
				cardsPlayedThisTurn = 0;
				currentTurn = poGame.Turn;
				firstMoveThisTurn = true;
			}
			else if ( ( cardsPlayedThisTurn % 5 ) == 0)
			{
				firstMoveThisTurn = true;
				MyStopwatch.Start();
			}
			else
			{
				MyStopwatch.Start();
			}

			// choose option
			List<PlayerTask> options = poGame.CurrentPlayer.Options();
			PlayerTask option = getRHMove(poGame, options);

			// check option result
			// poGame.Process(option);

			// return move

			cardsPlayedThisTurn++;
			MyStopwatch.Stop();
			if (MyStopwatch.ElapsedTicks > LongestTurnStopwatch.ElapsedTicks)
			{
				LongestTurnStopwatch = MyStopwatch;
			}
			return option;
		}



		private int? SelectTaskList()
		{
			// choose task list
			// 1. Hero Killed?
			// 2. Max. Dmg to Hero
			// 3. Max. Killed Minions
			int? selectedTaskList = null;
			int maxHeroLifeSteal = int.MinValue;
			int maxKilledMinions = 0;
			int opponentHeroHealthBefore = InitialPoGame.CurrentOpponent.Hero.Health;

			for (int? i = 0; i < OptionsDictionary.Count; i++)
			{
				Tuple<List<PlayerTask>, bool, int, int> anEntry = OptionsDictionary.GetValueOrDefault(i);

				// Hero killed?
				if (anEntry.Item2)
				{
					selectedTaskList = i;
					break;
				}
				// Dmg to Hero?
				if (anEntry.Item3 > maxHeroLifeSteal)
				{
					maxHeroLifeSteal = anEntry.Item3;
				}
				// KilledMinions
				if (anEntry.Item4 > maxKilledMinions)
				{
					maxKilledMinions = anEntry.Item4;
				}
			}

			if (selectedTaskList == null)
			{
				List<int?> firstSelectedLists = new List<int?>();
				foreach (int? i in OptionsDictionary.Keys)
				{
					Tuple<List<PlayerTask>, bool, int, int> anEntry = OptionsDictionary.GetValueOrDefault(i);
					if (anEntry.Item3 == maxHeroLifeSteal)
					{
						firstSelectedLists.Add(i);
					}
				}

				if (firstSelectedLists.Count == 1)
				{
					selectedTaskList = firstSelectedLists[0];
				}
				else
				{
					List<int?> secondSelectedTasksList = new List<int?>();
					foreach (int? i in firstSelectedLists)
					{
						Tuple<List<PlayerTask>, bool, int, int> anEntry = OptionsDictionary.GetValueOrDefault(i);
						if (anEntry.Item4 == maxKilledMinions)
						{
							secondSelectedTasksList.Add(i);
						}
					}

					if (secondSelectedTasksList.Count == 1)
					{
						selectedTaskList = secondSelectedTasksList[0];
					}
					else
					{
						int listNum = Rnd.Next(secondSelectedTasksList.Count);
						selectedTaskList = new int?(listNum);
					}
				}
			}

			return selectedTaskList;
		}

		private void AddEntryToOptionsDictionary(List<PlayerTask> taskList, POGame.POGame aPoGame)
		{
			bool heroKilled = aPoGame.State.Equals(SabberStoneCore.Enums.State.COMPLETE);
			int heroLifeSteal = InitialPoGame.CurrentOpponent.Hero.Health - aPoGame.CurrentOpponent.Hero.Health;
			int killedMinions = aPoGame.NumMinionsKilledThisTurn;

			OptionsDictionary.Add(OptionsDictionary.Count, Tuple.Create(taskList, heroKilled, heroLifeSteal, killedMinions));
		}
	}
}
