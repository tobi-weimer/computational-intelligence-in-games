﻿using System;
using System.Collections.Generic;
using SabberStoneCore.Tasks;

namespace SabberStoneCoreAi.Agent
{
	class ImprovedRandomAgent : AbstractAgent
	{
		private Random Rnd = new Random();

		public override void FinalizeAgent()
		{
			Console.WriteLine("MyFirstAgent Finalize Agent");
			// TODO implement
			// Nothing to do here yet
		}

		public override void FinalizeGame()
		{
			Console.WriteLine("MyFirstAgent Finalize Game");
			// TODO implement
			// Nothing to do here yet
		}

		private List<PlayerTask> getNotEndTurnTasks(List<PlayerTask> options)
		{
			List<PlayerTask> myOptions = new List<PlayerTask>(options.Count);
			foreach (PlayerTask aPlayerTask in options)
			{
				if (!aPlayerTask.PlayerTaskType.Equals(PlayerTaskType.END_TURN))
				{
					myOptions.Add(aPlayerTask);
				}
			}
			return myOptions;
		}

		/// <summary>
		/// get random action, but not PlayerTaskType.END_TURN while other options are available
		/// </summary>
		/// <param name="poGame"></param>
		/// <param name="options"></param>
		/// <returns></returns>
		private PlayerTask getImprovedRandom(POGame.POGame poGame, List<PlayerTask> options)
		{
			PlayerTask option = options[Rnd.Next(options.Count)];

			if (options.Count > 1)
			{
				List<PlayerTask> myOptions = getNotEndTurnTasks(options);
				option = myOptions[Rnd.Next(myOptions.Count)];
			}

			return option;
		}

		public override PlayerTask GetMove(POGame.POGame poGame)
		{
			// TODO implement

			// choose option
			List<PlayerTask> options = poGame.CurrentPlayer.Options();
			PlayerTask option = getImprovedRandom(poGame, options);

			// check option result
			// poGame.Process(option);

			// return move
			return option;
		}

		public override void InitializeAgent()
		{
			Console.WriteLine("MyFirstAgent Init Agent");
			Rnd = new Random();
			// TODO implement
		}

		public override void InitializeGame()
		{
			Console.WriteLine("MyFirstAgent Init Game");
			// TODO implement
			// Nothing to do here yet
		}
	}
}
