﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SabberStoneCore.Tasks;

namespace SabberStoneCoreAi.Agent
{
	class RollingHorizonEvolutionAgent : AbstractAgent
	{
		public const string AgentName = "Rolling Horizon Evolution Agent";
		private Random Rnd = new Random();
		private Stopwatch LongestTurnStopwatch = new Stopwatch();
		private Stopwatch MyStopwatch;

		private POGame.POGame InitialPoGame;

		// TODO static readonly?
		private const int MatingPoolSize = 50;
		/// <summary>
		/// List-PlayerTask - Hero Killed - Hero Life Steal - Killed Minions
		/// </summary>
		private Dictionary<int?, Tuple<List<PlayerTask>, bool, int, int>> Population;
		/// <summary>
		/// List-PlayerTask - Hero Killed - Hero Life Steal - Killed Minions
		/// </summary>
		private Dictionary<int?, Tuple<List<PlayerTask>, bool, int, int>> MatingPool;
		private List<List<PlayerTask>> Children;
		private int? ChoosenIndividual;

		private int CurrentTurn = -1;
		private bool FirstMoveThisTurn = false;
		private int CardsPlayedThisTurn;
		
		public override void FinalizeAgent()
		{
			Console.WriteLine("{0}: Finalize Agent", AgentName);
		}

		public override void FinalizeGame()
		{
			Console.WriteLine("{0}: Finalize Game", AgentName);
			Console.WriteLine("{0}: The Longest Turn took {1} s", AgentName, LongestTurnStopwatch.ElapsedMilliseconds/1_000.0d);
		}

		public override void InitializeAgent()
		{
			Console.WriteLine("{0}: Init Agent", AgentName);
			Rnd = new Random();
		}

		public override void InitializeGame()
		{
			Console.WriteLine("{0}: Init Game", AgentName);
		}

		public override PlayerTask GetMove(POGame.POGame poGame)
		{
			InitialPoGame = poGame;
			if (poGame.Turn != CurrentTurn)
			{
				MyStopwatch = Stopwatch.StartNew();
				Console.WriteLine("{0}: Game Board:", AgentName);
				Console.WriteLine(poGame.FullPrint());
				CardsPlayedThisTurn = 0;
				CurrentTurn = poGame.Turn;
				FirstMoveThisTurn = true;
			}
			//else if ((CardsPlayedThisTurn % 5) == 0)
			//{
			//	FirstMoveThisTurn = true;
			//	MyStopwatch.Start();
			//}
			else
			{
				MyStopwatch.Start();
			}

			// choose option
			List<PlayerTask> options = poGame.CurrentPlayer.Options();
			PlayerTask option = GetRHEAMove(options);

			CardsPlayedThisTurn++;
			MyStopwatch.Stop();
			Console.WriteLine("{0}: Play Card {1}", AgentName, option);
			Console.WriteLine("{0}: Whole turn took {1} s", AgentName, MyStopwatch.ElapsedMilliseconds/1_000.0d);
			if (MyStopwatch.ElapsedTicks > LongestTurnStopwatch.ElapsedTicks)
			{
				LongestTurnStopwatch = MyStopwatch;
			}
			return option;
		}


		/// <summary>
		/// get the next move using Rolling Horizon
		/// </summary>
		private PlayerTask GetRHEAMove(List<PlayerTask> options)
		{
			PlayerTask choosenTask = null;

			try
			{
				if (options.Count == 1)
				{
					choosenTask = options[0];
				}
				else
				{
					if (FirstMoveThisTurn)
					{
						List<PlayerTask> newTaskList = new List<PlayerTask>();
						CreateInitialPopulation();

						if (Population.Count > 2)
						{
							int maxTimeForGeneticStuff = options.Count * 500;
							if (maxTimeForGeneticStuff > 60_000) maxTimeForGeneticStuff = 60_000;
							Dictionary<int?, Tuple<List<PlayerTask>, bool, int, int>> oldPopulation = Population;
							int iterationsWithuotPopulationChange = 0;
							while (MyStopwatch.ElapsedMilliseconds < maxTimeForGeneticStuff && iterationsWithuotPopulationChange < 100)
							{
								Selection();
								Crossover();
								//Mutation1();
								Mutation2();
								Reproduction();

								bool populationChanged = false;
								foreach (Tuple<List<PlayerTask>, bool, int, int> individual in oldPopulation.Values)
								{
									if (!Population.ContainsValue(individual))
									{
										populationChanged = true;
										break;
									}
								}
								if (populationChanged) iterationsWithuotPopulationChange = 0;
								else iterationsWithuotPopulationChange++;
							}
						}

						ChoosenIndividual = SelectIndividualForTurn();
						FirstMoveThisTurn = false;
					}

					Tuple<List<PlayerTask>, bool, int, int> anEntry = Population.GetValueOrDefault(ChoosenIndividual);
					if (anEntry != null)
					{
						List<PlayerTask> taskList = anEntry.Item1;
						if (taskList != null && CardsPlayedThisTurn >= 0 && CardsPlayedThisTurn < taskList.Count)
						{
							choosenTask = taskList[CardsPlayedThisTurn];
						}
					}
					if (choosenTask != null && !(choosenTask is SabberStoneCore.Tasks.PlayerTasks.ChooseTask))
					{
						List<PlayerTask> taskList = new List<PlayerTask>();
						taskList.Add(choosenTask);
						try
						{
							POGame.POGame NewPoGame = InitialPoGame.Simulate(taskList).GetValueOrDefault(choosenTask);
							if (NewPoGame == null)
							{
								choosenTask = null;
							}
						}
						catch (Exception)
						{
							choosenTask = null;
						}
					}
					else if (choosenTask is SabberStoneCore.Tasks.PlayerTasks.ChooseTask)
					{
						choosenTask = null;
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("!!!Exception!!!");
				Console.WriteLine("Exception:         " + e);
				Console.WriteLine("Exception String:  " + e.ToString());
				Console.WriteLine("Exception Message: " + e.Message);
				Console.WriteLine("Exception Source : " + e.Source);
				Console.WriteLine("Exception Stack Trace:\n" + e.StackTrace);
			}

			if (choosenTask == null)
			{
				choosenTask = GetRandomPlayerTask(options);
			}

			return choosenTask;
		}

		private int? SelectIndividualForTurn()
		{
			// choose task list
			// 1. Hero Killed?
			// 2. Max. Dmg to Hero
			// 3. Max. Killed Minions
			int? selectedTaskList = null;
			int maxHeroLifeSteal = int.MinValue;
			int maxKilledMinions = 0;
			int opponentHeroHealthBefore = InitialPoGame.CurrentOpponent.Hero.Health;

			int populationCount = Population.Count;
			for (int? i = 0; i < populationCount; i++)
			{
				Tuple<List<PlayerTask>, bool, int, int> anEntry = Population.GetValueOrDefault(i);

				// Hero killed?
				if (anEntry.Item2)
				{
					selectedTaskList = i;
					break;
				}
				// Dmg to Hero?
				if (anEntry.Item3 > maxHeroLifeSteal)
				{
					maxHeroLifeSteal = anEntry.Item3;
				}
				// KilledMinions
				if (anEntry.Item4 > maxKilledMinions)
				{
					maxKilledMinions = anEntry.Item4;
				}
			}

			if (selectedTaskList == null)
			{
				List<int?> firstSelectedLists = new List<int?>();
				foreach (int? i in Population.Keys)
				{
					Tuple<List<PlayerTask>, bool, int, int> anEntry = Population.GetValueOrDefault(i);
					if (anEntry.Item3 == maxHeroLifeSteal)
					{
						firstSelectedLists.Add(i);
					}
				}

				if (firstSelectedLists.Count == 1)
				{
					selectedTaskList = firstSelectedLists[0];
				}
				else
				{
					List<int?> secondSelectedTasksList = new List<int?>();
					foreach (int? i in firstSelectedLists)
					{
						Tuple<List<PlayerTask>, bool, int, int> anEntry = Population.GetValueOrDefault(i);
						if (anEntry.Item4 == maxKilledMinions)
						{
							secondSelectedTasksList.Add(i);
						}
					}

					int secondSelectedTasksListCount = secondSelectedTasksList.Count;
					if (secondSelectedTasksListCount == 1)
					{
						selectedTaskList = secondSelectedTasksList[0];
					}
					else
					{
						int listNum = Rnd.Next(secondSelectedTasksListCount);
						selectedTaskList = new int?(listNum);
					}
				}
			}

			return selectedTaskList;
		}

		private void Selection()
		{
			// TODO implement selection - turnament selection, based on hero kill, hero life steal and minion kills
			MatingPool = new Dictionary<int?, Tuple<List<PlayerTask>, bool, int, int>>(MatingPoolSize);
			int populationCount = Population.Count;
			for (int i = 0; i < MatingPoolSize; i++)
			{
				int selection = Rnd.Next(populationCount);
				MatingPool.Add(i, Population.GetValueOrDefault(selection));
			}
		}

		private void Crossover()
		{
			Children = new List<List<PlayerTask>>(MatingPoolSize);

			int maxChildren = Population.Count < MatingPoolSize / 2 ? Population.Count : MatingPoolSize;
			for (int i = 0; i < maxChildren; i++)
			{
				int parentNo1 = Rnd.Next(MatingPool.Count);
				int parentNo2 = Rnd.Next(MatingPool.Count);
				Tuple<List<PlayerTask>, bool, int, int> parent1 = MatingPool.GetValueOrDefault(new int?(parentNo1));
				Tuple<List<PlayerTask>, bool, int, int> parent2 = MatingPool.GetValueOrDefault(new  int?(parentNo2));
				List<PlayerTask> parentList1 = parent1.Item1;
				List<PlayerTask> parentList2 = parent2.Item1;

				int parentList1Count = parentList1.Count;
				int parentList2Count = parentList2.Count;

				if (parentList1Count > 1 && parentList2Count > 1)
				{
					List<PlayerTask> child1 = new List<PlayerTask>((parentList1Count + parentList2Count) / 2);
					child1.AddRange(parentList1.GetRange(0, parentList1Count / 2));
					child1.AddRange(parentList2.GetRange(parentList2Count / 2, parentList2Count / 2));
					List<PlayerTask> child2 = new List<PlayerTask>((parentList1Count + parentList2Count) / 2);
					child2.AddRange(parentList2.GetRange(0, parentList2Count / 2));
					child2.AddRange(parentList1.GetRange(parentList1Count / 2, parentList1Count / 2));
					Children.Add(child1);
					Children.Add(child2);
				}
			}
		}

		private void Mutation1()
		{
			List<List<PlayerTask>> oldChildren = Children;
			Children = new List<List<PlayerTask>>(oldChildren.Count);
			foreach (List<PlayerTask> child in oldChildren)
			{
				List<PlayerTask> mutatedChild = new List<PlayerTask>(child);
				int random = Rnd.Next(mutatedChild.Count - 1);
				PlayerTask mutation = GetRandomPlayerTask(InitialPoGame.CurrentPlayer.Options());
				mutatedChild[random] = mutation;
				Children.Add(mutatedChild);
			}
		}

		private void Mutation2()
		{
			List<List<PlayerTask>> oldChildren = Children;
			Children = new List<List<PlayerTask>>(oldChildren.Count);
			foreach (List<PlayerTask> child in oldChildren)
			{
				List<PlayerTask> mutatedChild = new List<PlayerTask>(child);
				int addOrRemove = Rnd.Next(1);
				int random = Rnd.Next(mutatedChild.Count - 1);
				if (addOrRemove == 1)
				{
					PlayerTask mutation = GetRandomPlayerTask(InitialPoGame.CurrentPlayer.Options());
					mutatedChild.Insert(random, mutation);
					mutatedChild[random] = mutation;
				}
				else
				{
					mutatedChild.RemoveAt(random);
				}
				Children.Add(mutatedChild);

			}
		}

		private void Reproduction()
		{
			foreach (List<PlayerTask> child in Children)
			{
				int childCount = child.Count - 1;
				POGame.POGame currentPoGame = InitialPoGame;
				bool addIndividual = true;
				for (int i = 0; i < childCount; i++)
				{
					List<PlayerTask> currentPlayerTask = new List<PlayerTask>();
					currentPlayerTask.Add(child[i]);
					try
					{
						POGame.POGame nextPoGame = currentPoGame.Simulate(currentPlayerTask).GetValueOrDefault(child[i]);
						if (nextPoGame == null)
						{
							addIndividual = false;
							break;
						}
						else
						{
							currentPoGame = nextPoGame;
						}
					}
					catch (Exception)
					{
						break;
					}
				}
				if (addIndividual)
				{
					AddIndividualToPopulation(child, currentPoGame);
				}
			}

			ReducePopulation();
		}

		private void ReducePopulation()
		{
			// 1. Hero Killed?
			// 2. Max. Dmg to Hero
			// 3. Max. Killed Minions

			int populationCount = Population.Count;

			if (populationCount > 0)
			{
				int heroKillers = 0;
				List<int> heroLifeSteal = new List<int>(populationCount);
				List<int> killedMinions = new List<int>(populationCount);

				int opponentHeroHealthBefore = InitialPoGame.CurrentOpponent.Hero.Health;

				foreach (Tuple<List<PlayerTask>, bool, int, int> anEntry in Population.Values)
				{
					// Hero killed?
					if (anEntry.Item2)
					{
						heroKillers++;
					}

					// Dmg to Hero?
					heroLifeSteal.Add(opponentHeroHealthBefore - anEntry.Item3);

					// KilledMinions
					killedMinions.Add(anEntry.Item4);
				}
				heroLifeSteal.Sort();
				killedMinions.Sort();

				Dictionary<int?, Tuple<List<PlayerTask>, bool, int, int>> oldPopulation = Population;
				Population = new Dictionary<int?, Tuple<List<PlayerTask>, bool, int, int>>(MatingPoolSize * 2);

				if (heroKillers <= MatingPoolSize * 2)
				{
					int i = 0;
					List<Tuple<List<PlayerTask>, bool, int, int>> furtherIndividuals = new List<Tuple<List<PlayerTask>, bool, int, int>>();
					foreach (Tuple<List<PlayerTask>, bool, int, int> individual in oldPopulation.Values)
					{
						if (individual.Item2)
						{
							Population.Add(i++, individual);
						}
						else
						{
							furtherIndividuals.Add(individual);
						}
					}
					if (heroKillers < MatingPoolSize * 2)
					{
						int individualsToAdd = MatingPoolSize * 2 - Population.Count;
						int pos = MatingPoolSize * 2 - individualsToAdd;
						if (pos < 0 || pos > populationCount)
						{
							pos = 0;
						}
						int heroLifeStealMin = heroLifeSteal[pos];

						List<Tuple<List<PlayerTask>, bool, int, int>> removeIndividuals = new List<Tuple<List<PlayerTask>, bool, int, int>>();
						foreach (Tuple<List<PlayerTask>, bool, int, int> individual in furtherIndividuals)
						{
							if (individual.Item3 > heroLifeStealMin)
							{
								Population.Add(i++, individual);
								removeIndividuals.Add(individual);
								individualsToAdd--;
								if (individualsToAdd == 0)
								{
									break;
								}
							}
						}
						foreach (Tuple<List<PlayerTask>, bool, int, int> individual in removeIndividuals)
						{
							furtherIndividuals.Remove(individual);
						}

						if (individualsToAdd > 0)
						{
							pos = MatingPoolSize * 2 - individualsToAdd;
							if (pos < 0 || pos > populationCount)
							{
								pos = 0;
							}
							int minMinionsKilled = killedMinions[pos];
							removeIndividuals = new List<Tuple<List<PlayerTask>, bool, int, int>>();
							foreach (Tuple<List<PlayerTask>, bool, int, int> individual in oldPopulation.Values)
							{
								if (individual.Item4 >= minMinionsKilled)
								{
									Population.Add(i++, individual);
									removeIndividuals.Add(individual);
									individualsToAdd--;
									if (individualsToAdd == 0)
									{
										break;
									}
								}
							}
							foreach (Tuple<List<PlayerTask>, bool, int, int> individual in removeIndividuals)
							{
								furtherIndividuals.Remove(individual);
							}

							while (individualsToAdd > 0 && furtherIndividuals.Count > 0)
							{
								Tuple<List<PlayerTask>, bool, int, int> individual = furtherIndividuals[Rnd.Next(furtherIndividuals.Count)];
								Population.Add(i++, individual);
								furtherIndividuals.Remove(individual);
								individualsToAdd--;
							}
						}
					}
				}
				else
				{
					int heroLifeStealMin = heroLifeSteal[MatingPoolSize * 2];
					int i = 0;
					List<Tuple<List<PlayerTask>, bool, int, int>> furtherIndividuals = new List<Tuple<List<PlayerTask>, bool, int, int>>();
					foreach (Tuple<List<PlayerTask>, bool, int, int> individual in oldPopulation.Values)
					{
						if (individual.Item2 && individual.Item3 >= heroLifeStealMin)
						{
							Population.Add(i++, individual);
						}
						else
						{
							furtherIndividuals.Add(individual);
						}
					}
					if (Population.Count < MatingPoolSize * 2)
					{
						int individualsToAdd = MatingPoolSize * 2 - Population.Count;
						int pos = MatingPoolSize * 2 - individualsToAdd;
						if (pos < 0 || pos > populationCount)
						{
							pos = 0;
						}
						int minMinionsKilled = killedMinions[pos];
						List<Tuple<List<PlayerTask>, bool, int, int>> removeIndividuals = new List<Tuple<List<PlayerTask>, bool, int, int>>();
						foreach (Tuple<List<PlayerTask>, bool, int, int> individual in oldPopulation.Values)
						{
							if (individual.Item4 >= minMinionsKilled)
							{
								Population.Add(i++, individual);
								removeIndividuals.Add(individual);
								individualsToAdd--;
								if (individualsToAdd == 0)
								{
									break;
								}
							}
						}
						foreach (Tuple<List<PlayerTask>, bool, int, int> individual in removeIndividuals)
						{
							furtherIndividuals.Remove(individual);
						}

						while (individualsToAdd > 0 && furtherIndividuals.Count > 0)
						{
							Tuple<List<PlayerTask>, bool, int, int> individual = furtherIndividuals[Rnd.Next(furtherIndividuals.Count)];
							Population.Add(i++, individual);
							furtherIndividuals.Remove(individual);
							individualsToAdd--;
						}
					}
					else if (Population.Count > MatingPoolSize * 2)
					{
						int minMinionsKilled = killedMinions[MatingPoolSize * 2];
						furtherIndividuals = new List<Tuple<List<PlayerTask>, bool, int, int>>();
						oldPopulation = Population;
						Population = new Dictionary<int?, Tuple<List<PlayerTask>, bool, int, int>>(MatingPoolSize * 2);
						i = 0;
						foreach (Tuple<List<PlayerTask>, bool, int, int> individual in oldPopulation.Values)
						{
							if (individual.Item4 > minMinionsKilled)
							{
								Population.Add(i++, individual);
							}
							else
							{
								furtherIndividuals.Add(individual);
							}
						}

						while (Population.Count < MatingPoolSize * 2 && furtherIndividuals.Count > 0)
						{
							Tuple<List<PlayerTask>, bool, int, int> individual = furtherIndividuals[Rnd.Next(furtherIndividuals.Count)];
							Population.Add(i++, individual);
							furtherIndividuals.Remove(individual);
						}
					}
				}
			}
		}

		private void AddIndividualToPopulation(List<PlayerTask> taskList, POGame.POGame aPoGame)
		{
			bool heroKilled = aPoGame.State.Equals(SabberStoneCore.Enums.State.COMPLETE);
			int heroLifeSteal = InitialPoGame.CurrentOpponent.Hero.Health - aPoGame.CurrentOpponent.Hero.Health;
			int killedMinions = aPoGame.NumMinionsKilledThisTurn;

			Population.Add(Population.Count, Tuple.Create(taskList, heroKilled, heroLifeSteal, killedMinions));
		}

		private void CreateInitialPopulation()
		{
			List<PlayerTask> options = InitialPoGame.CurrentPlayer.Options();
			int optionsCount = options.Count;
			Population = new Dictionary<int?, Tuple<List<PlayerTask>, bool, int, int>>(MatingPoolSize * 2);

			for (int i = 0; i < MatingPoolSize * 2; i++)
			{
				POGame.POGame FinalPoGame = InitialPoGame.getCopy();
				List<PlayerTask> TaskList = new List<PlayerTask>();

				while (optionsCount > 0 && !FinalPoGame.State.Equals(SabberStoneCore.Enums.State.COMPLETE) && !FinalPoGame.State.Equals(SabberStoneCore.Enums.State.INVALID))
				{
					PlayerTask aTask = GetRandomPlayerTask(options);
					try
					{
						List<PlayerTask> taskList = new List<PlayerTask>();
						taskList.Add(aTask);
						POGame.POGame NewPoGame = FinalPoGame.Simulate(taskList).GetValueOrDefault(aTask);
						if (NewPoGame != null)
						{
							FinalPoGame = NewPoGame;
							TaskList.Add(aTask);
							options = FinalPoGame.CurrentPlayer.Options();
							optionsCount = options.Count;
						}
						else
						{
							taskList.RemoveAt(taskList.Count - 1);
							break;
						}
					}
					catch (Exception)
					{
						break;
					}
				}
				AddIndividualToPopulation(TaskList, FinalPoGame);
			}
	}

		private PlayerTask GetRandomPlayerTask(List<PlayerTask> options)
		{
			List<PlayerTask> validTasks = GetNotEndTurnTasks(options);
			if (validTasks.Count >= 1)
			{
				return validTasks[Rnd.Next(validTasks.Count)];
			}
			else
			{
				return options[0];
			}
		}

		private List<PlayerTask> GetNotEndTurnTasks(List<PlayerTask> options)
		{
			List<PlayerTask> myOptions = new List<PlayerTask>(options.Count);
			foreach (PlayerTask aPlayerTask in options)
			{
				if (!aPlayerTask.PlayerTaskType.Equals(PlayerTaskType.END_TURN))
				{
					myOptions.Add(aPlayerTask);
				}
			}
			return myOptions;
		}
	}
}
