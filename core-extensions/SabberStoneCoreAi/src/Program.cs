﻿using System;
using SabberStoneCore.Config;
using SabberStoneCore.Enums;
using SabberStoneCoreAi.POGame;
using SabberStoneCoreAi.Agent.ExampleAgents;
using SabberStoneCoreAi.Agent;

namespace SabberStoneCoreAi
{
	internal class Program
	{

		private static void Main(string[] args)
		{
			System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();

			int numberOfGames = 5; // 1 Match -> 2 Games -> takes about 20 minutes

			Console.WriteLine("Setup gameConfig");

			//todo: rename to Main
			GameConfig gameConfig1 = new GameConfig
			{
				StartPlayer = 1,
				Player1HeroClass = CardClass.MAGE,
				Player1Name = RollingHorizonEvolutionAgent.AgentName,
				Player2HeroClass = CardClass.MAGE,
				FillDecks = true,
				Logging = false
			};

			Console.WriteLine("Setup POGameHandler");
			AbstractAgent player1 = new RollingHorizonEvolutionAgent();
			AbstractAgent player2 = new RandomAgentLateEnd();
			var gameHandler1 = new POGameHandler(gameConfig1, player1, player2, debug:true);

			Console.WriteLine("PlayGame");
			gameHandler1.PlayGames(numberOfGames);
			GameStats gameStats1 = gameHandler1.getGameStats();

			GameConfig gameConfig2 = new GameConfig
			{
				StartPlayer = 2,
				Player1HeroClass = CardClass.MAGE,
				Player1Name = RollingHorizonEvolutionAgent.AgentName,
				Player2HeroClass = CardClass.MAGE,
				FillDecks = true,
				Logging = false
			};

			Console.WriteLine("Setup POGameHandler");
			var gameHandler2 = new POGameHandler(gameConfig2, player1, player2, debug: true);

			Console.WriteLine("PlayGame");
			gameHandler2.PlayGames(numberOfGames);
			GameStats gameStats2 = gameHandler2.getGameStats();

			Console.WriteLine("Results:");
			Console.WriteLine("Game Stats 1");
			gameStats1.printResults();
			Console.WriteLine();
			Console.WriteLine("Game Stats 2");
			gameStats2.printResults();


			Console.WriteLine("Test successful");

			stopwatch.Stop();

			Console.WriteLine("Time needed for the game: {0} s", stopwatch.ElapsedMilliseconds / 1_000.0d);

			Console.ReadLine();
		}
	}
}
